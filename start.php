<?php
/**
 * Blogs
 *
 * @package Blog
 *
 */

elgg_register_event_handler('init', 'system', 'blog_init');

/**
 * Init blog plugin.
 */
function blog_init() {
	
	elgg_register_library('elgg:blog', elgg_get_plugins_path() . 'blog_example/lib/blog.php');
	
	// add a site navigation item
	$item = new ElggMenuItem('blog', elgg_echo('blog:blogs'), 'blog/all');
	elgg_register_menu_item('site', $item);
	
	// routing of urls
	elgg_register_page_handler('blog', 'blog_page_handler');
	
	// override the default url to view a blog object
	elgg_register_entity_url_handler('object', 'blog', 'blog_url_handler');
	
	// register actions
	$action_path = elgg_get_plugins_path() . 'blog_example/actions/blog';
	elgg_register_action('blog/save', "$action_path/save.php");
	elgg_register_action('blog/delete', "$action_path/delete.php");
}

/**
 * Dispatches blog pages.
 * URLs take the form of
 *  All blogs:       blog/all
 *  User's blogs:    blog/owner/<username>
 *  Friends' blog:   blog/friends/<username>
 *  Blog post:       blog/view/<guid>/<title>
 *  New post:        blog/add/<guid>
 *  Edit post:       blog/edit/<guid>/<revision>
 *
 * Title is ignored
 *
 * @todo no archives for all blogs or friends
 *
 * @param array $page
 * @return NULL
 */
function blog_page_handler($page) {
	
	elgg_load_library('elgg:blog');

	// push all blogs breadcrumb
	elgg_push_breadcrumb(elgg_echo('blog:blogs'), "blog/all");

	if (!isset($page[0])) {
		$page[0] = 'all';
	}

	$page_type = $page[0];
	switch ($page_type) {
		case 'owner':
			elgg_register_title_button();
			$user = get_user_by_username($page[1]);
			$params = array(
				'title' => elgg_echo('blog:title:user_blogs', array($user->name)),
				'content' => "Here there will be the $user->name blogs.",
				'filter_context' => 'mine',
			);
			break;
		case 'friends':
			elgg_register_title_button();
			$user = get_user_by_username($page[1]);
			$params = array(
				'title' => elgg_echo('blog:title:friends', array($user->name)),
				'content' => "Here there will be the {$user->name}'s friends blogs",
				'filter_context' => 'friends',
			);
			break;
		case 'view':
			$params = blog_get_page_content_read($page[1]);
			break;
		case 'add':
			gatekeeper();
			$params = blog_get_page_content_edit($page_type, $page[1]);
			break;
		case 'edit':
			gatekeeper();
			$params = blog_get_page_content_edit($page_type, $page[1], $page[2]);
			break;
		case 'all':
		default:
			$title = elgg_echo('blog:title:all_blogs');
			$params = blog_get_page_content_list();
			break;
	}

	$body = elgg_view_layout('content', $params);

	echo elgg_view_page($params['title'], $body);
}

/**
 * Format and return the URL for blogs.
 *
 * @param ElggObject $entity Blog object
 * @return string URL of blog.
 */
function blog_url_handler($entity) {
	if (!$entity->getOwnerEntity()) {
		// default to a standard view if no owner.
		return FALSE;
	}

	$friendly_title = elgg_get_friendly_title($entity->title);

	return "blog/view/{$entity->guid}/$friendly_title";
}
